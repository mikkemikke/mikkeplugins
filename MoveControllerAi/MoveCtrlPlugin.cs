﻿using AIChara;
using BepInEx;
using BepInEx.Configuration;
using BepInEx.Logging;
using HarmonyLib;
using Studio;
using UnityEngine;

namespace MoveController {
    [BepInPlugin(GUID, "Move Controller AI", VERSION)]
    [BepInProcess("StudioNEOV2")]
    public class MoveCtrlPlugin : BaseUnityPlugin {
        public const string GUID = "mikke.MoveControllerAI";
        public const string VERSION = "1.8.0";

        public static ConfigFile ConfigFile;
        public const string MoveCtrlConfigName = "Move Controller settings";

        public static ManualLogSource Log;


        public void OnLevelWasLoaded(int level) {
            Log = Logger;

            StartMod();
        }

        private void Awake() {
            Harmony.CreateAndPatchAll(typeof(MoveCtrlPlugin));
        }

        [HarmonyPostfix]
        [HarmonyPatch(typeof(AddObjectFemale), "Add", typeof(ChaControl), typeof(OICharInfo), typeof(ObjectCtrlInfo), typeof(TreeNodeObject), typeof(bool), typeof(int))]
        [HarmonyPatch(typeof(AddObjectMale), "Add", typeof(ChaControl), typeof(OICharInfo), typeof(ObjectCtrlInfo), typeof(TreeNodeObject), typeof(bool), typeof(int))]
        [HarmonyPatch(typeof(ChaControl), "ChangeNowCoordinate", typeof(ChaFileCoordinate), typeof(bool), typeof(bool))]
        public static void UpdateAccessoryNodes() {
            AccessoryCtrlService.Instance.UpdateNodes();
        }


        public void StartMod() {
            
            ConfigFile = Config;
            new GameObject(GUID).AddComponent<MoveCtrlWindow>();
        }
    }
}