﻿using System;
using System.Collections.Generic;
using AIChara;
using Studio;
using UnityEngine;
using UnityEngine.UI;

namespace MoveController {
    public class AccessoryCtrlService {

        public static AccessoryCtrlService Instance;

        private Dictionary<string, Tuple<int, int>> parentNodeMap = new Dictionary<string, Tuple<int, int>>() {
            {ChaAccessoryDefine.AccessoryParentKey.N_Hair_pony.ToString(), new Tuple<int, int>(0, 0)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Hair_twin_L.ToString(), new Tuple<int, int>(0, 1)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Hair_twin_R.ToString(), new Tuple<int, int>(0, 2)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Hair_pin_L.ToString(), new Tuple<int, int>(0, 3)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Hair_pin_R.ToString(), new Tuple<int, int>(0, 4)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Head_top.ToString(), new Tuple<int, int>(1, 0)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Hitai.ToString(), new Tuple<int, int>(1, 1)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Head.ToString(), new Tuple<int, int>(1, 2)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Face.ToString(), new Tuple<int, int>(1, 3)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Earring_L.ToString(), new Tuple<int, int>(2, 0)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Earring_R.ToString(), new Tuple<int, int>(2, 1)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Megane.ToString(), new Tuple<int, int>(2, 2)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Nose.ToString(), new Tuple<int, int>(2, 3)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Mouth.ToString(), new Tuple<int, int>(2, 4)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Neck.ToString(), new Tuple<int, int>(3, 0)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Chest_f.ToString(), new Tuple<int, int>(3, 1)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Chest.ToString(), new Tuple<int, int>(3, 2)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Tikubi_L.ToString(), new Tuple<int, int>(4, 0)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Tikubi_R.ToString(), new Tuple<int, int>(4, 1)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Back.ToString(), new Tuple<int, int>(4, 2)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Back_L.ToString(), new Tuple<int, int>(4, 3)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Back_R.ToString(), new Tuple<int, int>(4, 4)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Waist.ToString(), new Tuple<int, int>(5, 0)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Waist_f.ToString(), new Tuple<int, int>(5, 1)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Waist_b.ToString(), new Tuple<int, int>(5, 2)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Waist_L.ToString(), new Tuple<int, int>(5, 3)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Waist_R.ToString(), new Tuple<int, int>(5, 4)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Leg_L.ToString(), new Tuple<int, int>(6, 0)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Knee_L.ToString(), new Tuple<int, int>(6, 1)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Ankle_L.ToString(), new Tuple<int, int>(6, 2)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Foot_L.ToString(), new Tuple<int, int>(6, 3)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Leg_R.ToString(), new Tuple<int, int>(6, 4)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Knee_R.ToString(), new Tuple<int, int>(6, 5)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Ankle_R.ToString(), new Tuple<int, int>(6, 6)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Foot_R.ToString(), new Tuple<int, int>(6, 7)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Shoulder_L.ToString(), new Tuple<int, int>(7, 0)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Elbo_L.ToString(), new Tuple<int, int>(7, 1)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Arm_L.ToString(), new Tuple<int, int>(7, 2)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Wrist_L.ToString(), new Tuple<int, int>(7, 3)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Shoulder_R.ToString(), new Tuple<int, int>(7, 4)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Elbo_R.ToString(), new Tuple<int, int>(7, 5)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Arm_R.ToString(), new Tuple<int, int>(7, 6)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Wrist_R.ToString(), new Tuple<int, int>(7, 7)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Hand_L.ToString(), new Tuple<int, int>(8, 0)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Index_L.ToString(), new Tuple<int, int>(8, 1)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Middle_L.ToString(), new Tuple<int, int>(8, 2)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Ring_L.ToString(), new Tuple<int, int>(8, 3)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Hand_R.ToString(), new Tuple<int, int>(8, 4)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Index_R.ToString(), new Tuple<int, int>(8, 5)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Middle_R.ToString(), new Tuple<int, int>(8, 6)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Ring_R.ToString(), new Tuple<int, int>(8, 7)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Dan.ToString(), new Tuple<int, int>(9, 0)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Kokan.ToString(), new Tuple<int, int>(9, 1)},
            {ChaAccessoryDefine.AccessoryParentKey.N_Ana.ToString(), new Tuple<int, int>(9, 2)}
        };

        private readonly Color accColor = new Color(0f, 0.75f, 0.75f);
        private readonly MoveObjectService moveObjectService;

        public readonly Dictionary<TreeNodeObject, AccMoveInfo> AccMoveInfos = new Dictionary<TreeNodeObject, AccMoveInfo>();
        public AccMoveInfo Current = null;
        
        private bool displayNodes;

        private Quaternion initialRotation;
        private Vector3 initialPosition = Vector3.zero;

        internal AccessoryCtrlService(MoveObjectService moveObjectService) {
            this.moveObjectService = moveObjectService;
            Instance = this;
        }

        public bool IsAccessoryControl() {
            return Current != null && Current.Node!=null && Studio.Studio.Instance.treeNodeCtrl.selectNode == Current.Node;
        }

        public void ToggleNodes(Button accCtrlButton) {
            if (displayNodes == false) {
                EnableAccNodes();
                accCtrlButton.image.color=Color.green;
                displayNodes = true;
            } else {
                DisableAccNodes();
                accCtrlButton.image.color=Color.white;
                displayNodes = false;
            }
        }

        public void UpdateNodes() {
            if (displayNodes) {
                DisableAccNodes();
                EnableAccNodes();
            }
        }

        private void DisableAccNodes() {
            TreeNodeCtrl treeNodeCtrl = Studio.Studio.Instance.treeNodeCtrl;
            var nodes = GameObject.Find("StudioScene").transform.Find("Canvas Object List/Image Bar").GetComponentsInChildren<TreeNodeObject>(true);

            foreach (var node in nodes) {
                if (AccMoveInfos.ContainsKey(node)) {
                    if (treeNodeCtrl.CheckSelect(node)) {
                        treeNodeCtrl.SelectSingle(node, true);
                    }

                    node.enableDelete = true;
                    node.enableChangeParent = true;
                    treeNodeCtrl.DeleteNode(node);
                }
            }
        }

        private void EnableAccNodes() {
            var nodes = GameObject.Find("StudioScene").transform.Find("Canvas Object List/Image Bar").GetComponentsInChildren<TreeNodeObject>(true);

            foreach (var node in nodes) {
                if (Studio.Studio.Instance.dicInfo.TryGetValue(node, out var info)) {
                    if (info is OCIChar chara) {
                        ProcessCharacterNode(chara, node);
                    }
                }
            }
        }

        private void ProcessCharacterNode(OCIChar chara, TreeNodeObject charaNode) {
            TreeNodeCtrl treeNodeCtrl = Studio.Studio.Instance.treeNodeCtrl;
            var charaInfo = chara.charInfo;
            var accessories = charaInfo.objAccessory;
            var listInfo = charaInfo.infoAccessory;

            for (int index = 0; index < accessories.Length; index++) {
                if (accessories[index] == null) {
                    continue;
                }

                var parentKey = charaInfo.chaFile.coordinate.accessory.parts[index].parentKey;

                if (!parentNodeMap.ContainsKey(parentKey)) {
                    UnityEngine.Debug.LogError("Could not find parentNode with key " + parentKey);
                    continue;
                }

                (int item1, int item2) = parentNodeMap[parentKey];
                var parentNode = charaNode.child[item1].child[item2];

                var nodeName = listInfo[index].Name;

                var newNode = treeNodeCtrl.AddNode(nodeName);
                newNode.baseColor = accColor;
                newNode.colorSelect = accColor;

                SetParent(newNode, parentNode);

                newNode.enableChangeParent = false;
                newNode.enableCopy = false;
                newNode.enableDelete = false;
                newNode.enableAddChild = false;
                newNode.onDelete = () => AccMoveInfos.Remove(newNode);

                AccMoveInfos.Add(newNode, new AccMoveInfo(chara, index, newNode));
            }
        }

        private static GameObject GetAccessoryObject(GameObject slotObject) {

            var tempObj = slotObject;
            //try {
                while (tempObj.transform.childCount > 0) {
                    var nMove = tempObj.transform.Find("N_move");
                    if(nMove!=null) return nMove.gameObject;

                    tempObj = tempObj.transform.GetChild(0).gameObject;
                }

                throw new ArgumentException("Could not locate Accessory control object");
        }

        public void MoveAccessory(Vector3 input) {
            var accObject = GetAccessoryObject(Current.Chara.charInfo.objAccessory[Current.Index]);
            var amount = input * moveObjectService.moveSpeedFactor;
            var charaInfo = Current.Chara.charInfo;

            MoveAcc(charaInfo, accObject, amount);
        }

        public void RotateAccessoryInWorld(Vector3 rotAmount, bool useSpeedfactor) {
            var slotObject = Current.Chara.charInfo.objAccessory[Current.Index];
            var accObject = GetAccessoryObject(slotObject);
            
            var rof = useSpeedfactor ? rotAmount * moveObjectService.rotationSpeedFactor : rotAmount;

            accObject.transform.Rotate(rof,Space.World);
        }
        
        public void RotateAccessoryByCamera(Vector3 angle, float rotAmount, bool useSpeedfactor) {
            var slotObject = Current.Chara.charInfo.objAccessory[Current.Index];
            var accObject = GetAccessoryObject(slotObject);
            
            var rof = useSpeedfactor ? rotAmount * moveObjectService.rotationSpeedFactor : rotAmount;

            accObject.transform.Rotate(angle,rof,Space.World);
        }

        private void RotateAcc(ChaControl charaInfo, GameObject accObject, Quaternion newRotation) {
            Quaternion startRotation = accObject.transform.localRotation;
            accObject.transform.localRotation = newRotation;

            var change = (newRotation.eulerAngles - startRotation.eulerAngles);
            charaInfo.chaFile.coordinate.accessory.parts[Current.Index].addMove[0, 1] += change;
        }

        public void RotateAccessory(Vector3 rotAmount, bool useSpeedfactor) {
            var slotObject = Current.Chara.charInfo.objAccessory[Current.Index];

            var accObject = GetAccessoryObject(slotObject);

            var charaInfo = Current.Chara.charInfo;
            var rof = useSpeedfactor ? rotAmount * moveObjectService.rotationSpeedFactor : rotAmount;

            Quaternion startRotation = accObject.transform.localRotation;

            Quaternion newRotation = startRotation * Quaternion.Euler(rof);
            RotateAcc(charaInfo, accObject, newRotation);
        }

        public void InitUndoRotate() {
            var accObject = GetAccessoryObject(Current.Chara.charInfo.objAccessory[Current.Index]);
            initialRotation = accObject.transform.localRotation;
        }

        public void CreateUndoRotate() {
            var accObject = GetAccessoryObject(Current.Chara.charInfo.objAccessory[Current.Index]);
            var current = accObject.transform.localRotation;
            var start = initialRotation;

            var command = new AccessoryCommand(() => RotateAcc(Current.Chara.charInfo, accObject, start),
                () => RotateAcc(Current.Chara.charInfo, accObject, current));

            UndoRedoManager.Instance.Push(command);
        }

        public void InitUndoMove() {
            var accObject = GetAccessoryObject(Current.Chara.charInfo.objAccessory[Current.Index]);
            initialPosition = accObject.transform.position;
        }

        public void CreateUndoMove() {
            var accObject = GetAccessoryObject(Current.Chara.charInfo.objAccessory[Current.Index]);
            var current = accObject.transform.position;
            var change = current - initialPosition;

            var command = new AccessoryCommand(() => MoveAcc(Current.Chara.charInfo, accObject, -change),
                () => MoveAcc(Current.Chara.charInfo, accObject, change));

            UndoRedoManager.Instance.Push(command);
        }


        private void MoveAcc(ChaControl charaInfo, GameObject accObject, Vector3 amount) {
            var orgPos = accObject.transform.localPosition;
            accObject.transform.Translate(amount, Space.World);

            var newPos = accObject.transform.localPosition;
            var change = (newPos - orgPos) * 10f;

            charaInfo.chaFile.coordinate.accessory.parts[Current.Index].addMove[0, 0] += change;
        }

        private void SetParent(TreeNodeObject node, TreeNodeObject parent) {
            TreeNodeCtrl treeNodeCtrl = Studio.Studio.Instance.treeNodeCtrl;
            if (node == null) {
                return;
            }

            if (!node.enableChangeParent) {
                return;
            }

            if (treeNodeCtrl.CheckNode(node) && parent == null) {
                return;
            }

            if (!node.SetParent(parent)) {
                return;
            }

            treeNodeCtrl.RefreshHierachy();
        }
    }

    public class AccMoveInfo {
        public OCIChar Chara { get; }
        public int Index { get; }

        public TreeNodeObject Node { get; }

        public AccMoveInfo(OCIChar chara, int index, TreeNodeObject node) {
            Chara = chara;
            Index = index;
            Node = node;
        }
    }

    public class AccessoryCommand : Studio.ICommand {

        private readonly Action undo;
        private readonly Action redo;
        
        public AccessoryCommand(Action undo, Action redo) {
            this.undo = undo;
            this.redo = redo;
        }

        public void Do() {
            UnityEngine.Debug.LogError(GetType().Name + ".Do() should not be invoked");
        }

        public void Undo() {
            undo.Invoke();
        }

        public void Redo() {
            redo.Invoke();
        }
    }
}