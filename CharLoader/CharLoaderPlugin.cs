using System.Collections;
using System.IO;
using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using KKAPI.Maker;
using KKAPI.Studio;

namespace CharLoader {
    [BepInPlugin(GUID, "Character Loader", VERSION)]
    [BepInDependency("GarryWu.HS2WearCustom", BepInDependency.DependencyFlags.SoftDependency)]
    [BepInDependency("gravydevsupreme.xunity.autotranslator", BepInDependency.DependencyFlags.SoftDependency)]
    public class CharLoaderPlugin : BaseUnityPlugin {
        public const string GUID = "mikke.Charloader";
        internal const string VERSION = "1.4.2";

        internal static DirectoryInfo FemaleBaseDir = new DirectoryInfo(UserData.Path + "chara/female");
        internal static DirectoryInfo MaleBaseDir = new DirectoryInfo(UserData.Path + "chara/male");

        internal static readonly string VariantsDirName = "Variants";

        private bool replaceStudioButtons;
        private int numRows;
        private ConfigEntry<bool> showMakerFolders;
        internal static ConfigEntry<bool> CloseStudioWindow;
        internal static bool EnableCharacterReordering;

        public void Start() {
            var replaceButtons = Config.AddSetting("Config", "Replace menu buttons", false,
                "Replace the standard buttons to add female and male characters to the game with a single Characters button");
            replaceStudioButtons = replaceButtons.Value;

            var numRowsSetting = Config.AddSetting("Config", "Chars per row.", 4, new
                ConfigDescription("Number of character cards per row in the Studio Character Loader. Takes effect on restart",
                    new AcceptableValueRange<int>(2, 6)));
            numRows = numRowsSetting.Value;

            showMakerFolders = Config.AddSetting("Config", "Maker folder list",
                true, "Show or hide folder list in the character maker when saving or loading characters");

            CloseStudioWindow = Config.AddSetting("Config", "Close window after select",
                true, "Close the Character Loader window in studio after loading a character");
            var reordering = Config.AddSetting("Config", "Studio character card reordering",
                false,
                "Allow manually reordering the Studio character list. This is done by manipulation the card file timestamps, " +
                "so the order will change also the character maker and windows explorer. Takes effect on restart");
            EnableCharacterReordering = reordering.Value;

            Harmony.CreateAndPatchAll(typeof(CharLoaderMaker));
        }

        private CharLoaderStudio charLoaderStudio;
        private CharLoaderMaker charLoaderMaker;

        public void OnLevelWasLoaded() {
            if (StudioAPI.InsideStudio) {
                SpawnStudioGui();
            } else {
                StartCoroutine(SpawnMakerGuiCO());
            }
        }

        private void SpawnStudioGui() {
            var old = gameObject.GetComponent<CharLoaderStudio>();
            if (old != null) {
                Destroy(old);
            }

            charLoaderStudio = gameObject.AddComponent<CharLoaderStudio>();
            charLoaderStudio.SpawnGui(replaceStudioButtons, numRows);
        }

        public IEnumerator SpawnMakerGuiCO() {
            CharLoaderMaker.IsSpawned = false;
            // Wait a few frames for the base game to load in to avoid conflicts loading default character before we scan directories
            yield return null;
            yield return null;
            yield return null;
            yield return null;
            yield return null;

            if (!MakerAPI.InsideMaker) {
                yield break;
            }

            charLoaderMaker = gameObject.AddComponent<CharLoaderMaker>();
            charLoaderMaker.SpawnGui(showMakerFolders);
        }
    }
}