﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using AIChara;
using ExtensibleSaveFormat;
using GravureAI;
using KKAPI.Utilities;
using Studio;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using BackgroundCtrl = Studio.BackgroundCtrl;
#if HS2
using WearCustom;
#elif AI
using AIWearCustom;
#endif

namespace CharLoader {
    public class CharLoaderStudio : MonoBehaviour {
        private Canvas GUI;
        private Canvas replaceCardGUI;
        private Canvas frameCanvas;

        private Transform confirmationPanel;

        private GameObject charButton;
        private GameObject menuButton;
        private Toggle closeToggle;
        private Text genderButtonText;

        private List<GameObject> buttonPanels = new List<GameObject>();
        private List<GameObject> variantPanels = new List<GameObject>();

        private bool wearCustomActive;
        private Type studioCharaListUtilType;

        private Color selectedColor = new Color(0.5f, 1, 1);

        delegate bool TranslateName(string text, int scope, out string translatedText);

        private TranslateName translateName;

        private bool femaleActive = true;

        private DirectoryInfo currentDir;
        private DirectoryInfo currentVariantDir;
        private Button selectedCard;
        private bool selectedIsVariant;

        private FileInfo selectedCharaFile;
        private FileInfo charaFileToReplace;
        private int fgIndex;
        private int bgIndex;
        private Texture2D currentBackground;

        internal void SpawnGui(bool replaceStudioButtons, int rows) {
            CheckWearCustom();
            SetUpNameTranslation();

            AssetBundle bundle = AssetBundle.LoadFromMemory(CharLoaderRes.loaderres);
            var obj = Instantiate(bundle.LoadAsset<GameObject>("CharLoaderCanvas"));
            GUI = obj.GetComponent<Canvas>();
            GUI.gameObject.SetActive(false);
            charButton = bundle.LoadAsset<GameObject>("CharButton2");

            confirmationPanel = GUI.transform.Find("ConfirmationPanel");

            replaceCardGUI = Instantiate(bundle.LoadAsset<GameObject>("ReplaceCardCanvas")).GetComponent<Canvas>();
            replaceCardGUI.gameObject.SetActive(false);

            frameCanvas = Instantiate(bundle.LoadAsset<GameObject>("FrameCanvas")).GetComponent<Canvas>();
            frameCanvas.gameObject.SetActive(false);
            frameCanvas.name = "VectorCanvas";

            bundle.Unload(false);

            float rowCount = rows;
            float scalefactor = 1;
            scalefactor = (4f / rowCount);

            var viewport = GUI.transform.Find("MainPanel/Body/CharView/Viewport/Content");
            viewport.GetComponent<GridLayoutGroup>().constraintCount = rows;
            viewport.transform.localScale = new Vector3(scalefactor, scalefactor, 1);

            var refreshButton = GUI.transform.Find("MainPanel/PanelHeader/RefreshButton").GetComponent<Button>();
            refreshButton.onClick.AddListener(() => ListCharsInDir(currentDir, false));

            var folderButton = GUI.transform.Find("MainPanel/PanelHeader/FolderButton").GetComponent<Button>();
            folderButton.onClick.AddListener(() => { Process.Start(currentDir.FullName); });

            var genderButton = GUI.transform.Find("MainPanel/PanelHeader/GenderButton").GetComponent<Button>();
            genderButtonText = genderButton.transform.Find("Text").GetComponent<Text>();
            genderButtonText.text = "Male";
            genderButton.onClick.AddListener(() => ToggleGender());

            var exitButton = GUI.transform.Find("MainPanel/PanelHeader/ExitButton").GetComponent<Button>();
            exitButton.onClick.AddListener(() => { closeAction.Invoke(); });
            closeToggle = GUI.transform.Find("MainPanel/CloseToggle").GetComponent<Toggle>();
            if (closeToggle != null && CharLoaderPlugin.CloseStudioWindow.Value == false) {
                closeToggle.isOn = false;
            }

            if (CharLoaderPlugin.EnableCharacterReordering) {
                Button moveCardForwardButton = GUI.transform.Find("MainPanel/MoveCardForwardButton").GetComponent<Button>();
                moveCardForwardButton.gameObject.SetActive(true);
                moveCardForwardButton.onClick.AddListener(() => MoveCardThroughTime(-1));

                Button moveCardBackButton = GUI.transform.Find("MainPanel/MoveCardBackButton").GetComponent<Button>();
                moveCardBackButton.gameObject.SetActive(true);
                moveCardBackButton.onClick.AddListener(() => MoveCardThroughTime(1));
            }


            Button deleteCardButton = GUI.transform.Find("MainPanel/DeleteCardButton").GetComponent<Button>();
            deleteCardButton.onClick.AddListener(() => {
                if (!File.Exists(selectedCharaFile.FullName)) {
                    return;
                }

                ShowConfirmationPanel("Are you sure you want to delete this card?", selectedCharaFile.FullName,
                    () => {
                        File.Delete(selectedCharaFile.FullName);
                        ListCharsInDir(currentDir, false);
                    });
            });

            Button replaceCardButton = GUI.transform.Find("MainPanel/ReplaceCardButton").GetComponent<Button>();

            replaceCardButton.onClick.AddListener(() => {
                if (selectedCharaFile == null) {
                    return;
                }

                charaFileToReplace = selectedCharaFile;
                ShowConfirmationPanel("Are you sure you want to replace this image?\nCharacter data will not be changed", selectedCharaFile.FullName,
                    () => {
                        closeAction.Invoke();
                        ShowCardReplacePanel();
                    });
            });

            SetupReplacerGui();
            CreateMenuButton(replaceStudioButtons);
            LoadDirList(CharLoaderPlugin.FemaleBaseDir);
        }

        private void ShowCardReplacePanel() {
            replaceCardGUI.gameObject.SetActive(true);
            frameCanvas.gameObject.SetActive(true);
            if (replaceCardGUI.transform.Find("ReplaceCardPanel/BackgroundToggle")?.GetComponent<Toggle>()?.isOn == true) {
                ToggleBackground(true);
                DisplayBackground(currentBackground);
            }
        }

        private void ShowConfirmationPanel(string message, string imgPath, Action action) {
            Button okButton = confirmationPanel.transform.Find("OkButton").GetComponent<Button>();
            okButton.onClick.ActuallyRemoveAllListeners();
            okButton.onClick.AddListener(() => {
                action.Invoke();
                confirmationPanel.gameObject.SetActive(false);
            });
            Text text = confirmationPanel.transform.Find("Text").GetComponent<Text>();
            text.text = message;
            RawImage rawImage = confirmationPanel.transform.Find("CardImg").GetComponent<RawImage>();
            Texture2D tex = PngAssist.LoadTexture(imgPath);
            rawImage.texture = tex;
            confirmationPanel.gameObject.SetActive(true);
        }

        private void SetupReplacerGui() {
            var fgImage = frameCanvas.transform.Find("FGImg");

            var fgFrames = GetFrameFiles("Front");

            var ftoggle = replaceCardGUI.transform.Find("ReplaceCardPanel/ForegroundToggle");

            if (fgFrames.Length > 0) {
                ftoggle.GetComponent<Toggle>().onValueChanged.AddListener(val => fgImage.gameObject.SetActive(val));
                var fileData = File.ReadAllBytes(fgFrames[fgIndex].FullName);
                var img = fgImage.GetComponent<RawImage>();
                var tex = new Texture2D(2, 2);
                tex.LoadImage(fileData);
                img.texture = tex;
                fgImage.gameObject.SetActive(true);

                //fgtoggle:
                var fbutton = ftoggle.Find("Buttons/ForwardButton").GetComponent<Button>();
                fbutton.onClick.AddListener(() => {
                    fgIndex = (fgIndex + 1) % fgFrames.Length;
                    tex.LoadImage(File.ReadAllBytes(fgFrames[fgIndex].FullName));
                    img.texture = tex;
                });

                var bbutton = ftoggle.Find("Buttons/BackButton").GetComponent<Button>();
                bbutton.onClick.AddListener(() => {
                    fgIndex = (fgIndex - 1) % fgFrames.Length;
                    if (fgIndex < 0) {
                        fgIndex = fgFrames.Length - 1;
                    }

                    tex.LoadImage(File.ReadAllBytes(fgFrames[fgIndex].FullName));
                    img.texture = tex;
                });
            } else {
                fgImage.gameObject.SetActive(false);
                var toggle = ftoggle.GetComponent<Toggle>();
                toggle.isOn = false;
                toggle.interactable = false;
            }

            //Backgrounds
            var bgtoggle = replaceCardGUI.transform.Find("ReplaceCardPanel/BackgroundToggle");
            bgtoggle.gameObject.SetActive(true);

            var backgrounds = GetFrameFiles("Back");
            if (backgrounds.Length > 0) {
                var tex = new Texture2D(2, 2);
                bgtoggle.GetComponent<Toggle>().onValueChanged.AddListener(val => {
                    tex.LoadImage(File.ReadAllBytes(backgrounds[bgIndex].FullName));
                    ToggleBackground(val);
                    if (val) {
                        DisplayBackground(tex);
                    }
                });

                var fbutton = bgtoggle.Find("Buttons/ForwardButton").GetComponent<Button>();
                fbutton.onClick.AddListener(() => {
                    bgIndex = (bgIndex + 1) % backgrounds.Length;
                    tex.LoadImage(File.ReadAllBytes(backgrounds[bgIndex].FullName));
                    DisplayBackground(tex);
                });

                var bbutton = bgtoggle.Find("Buttons/BackButton").GetComponent<Button>();
                bbutton.onClick.AddListener(() => {
                    bgIndex = (bgIndex - 1) % backgrounds.Length;
                    if (bgIndex < 0) {
                        bgIndex = backgrounds.Length - 1;
                    }

                    tex.LoadImage(File.ReadAllBytes(backgrounds[bgIndex].FullName));
                    DisplayBackground(tex);
                });
            }

            //savebutton
            var savebutton = replaceCardGUI.transform.Find("ReplaceCardPanel/SaveButton").GetComponent<Button>();
            savebutton.onClick.AddListener(() => {
                StartCoroutine(ReplaceCardImage(charaFileToReplace));
                replaceCardGUI.gameObject.SetActive(false);
                charaFileToReplace = null;
            });

            //cancel
            var cancelbutton = replaceCardGUI.transform.Find("ReplaceCardPanel/CancelButton").GetComponent<Button>();
            cancelbutton.onClick.AddListener(() => {
                replaceCardGUI.gameObject.SetActive(false);
                frameCanvas.gameObject.SetActive(false);
                charaFileToReplace = null;
                ToggleBackground(false);
            });

            MovableWindow.makeWindowMovable((RectTransform)replaceCardGUI.transform.Find("ReplaceCardPanel"));
        }

        private static FileInfo[] GetFrameFiles(string frameType) {
            FileInfo[] fgFrames;
#if HS2
            fgFrames = new DirectoryInfo(DefaultData.Path + "cardframe/"+frameType).GetFiles("*.png")
                .Concat(new DirectoryInfo(UserData.Path + "cardframe/"+frameType).GetFiles("*.png")).ToArray();
#elif AI
            fgFrames = new DirectoryInfo(UserData.Path + "cardframe/" + frameType).GetFiles("*.png").ToArray();
#endif
            return fgFrames;
        }

        private void ToggleBackground(bool showBackground) {
            var bgCtrl = GameObject.Find("StudioScene/Camera/CameraSet/MainCamera/p_N_stneo_haikei_00")?.GetComponent<BackgroundCtrl>();
            if (bgCtrl == null) {
                UnityEngine.Debug.LogError("Could not find BackgroundCtrl");
                return;
            }

            if (Singleton<Studio.Studio>.Instance.sceneInfo.background.IsNullOrEmpty()) {
                bgCtrl.isVisible = showBackground;
                if (showBackground) {
                    MeshRenderer renderer = bgCtrl.GetPrivateField<MeshRenderer>("meshRenderer");
                    var texture = renderer.material.mainTexture?.ToTexture2D();
                    if (texture == null) {
                        texture = new Texture2D(1920, 1080);
                    }

                    Color[] pixels = Enumerable.Repeat(Camera.main.backgroundColor, texture.width * texture.height).ToArray();
                    texture.SetPixels(pixels);
                    texture.Apply();
                    renderer.material.SetTexture(MainTex, texture);
                }
            }

            if (showBackground == false && !Singleton<Studio.Studio>.Instance.sceneInfo.background.IsNullOrEmpty()) {
                bgCtrl.Load(Singleton<Studio.Studio>.Instance.sceneInfo.background);
            }
        }

        private void DisplayBackground(Texture2D bgImage) {
            currentBackground = bgImage;
            var backgroundCtrl = GameObject.Find("StudioScene/Camera/CameraSet/MainCamera/p_N_stneo_haikei_00")?.GetComponent<BackgroundCtrl>();
            if (backgroundCtrl == null) {
                return;
            }

            MeshRenderer renderer = backgroundCtrl.GetPrivateField<MeshRenderer>("meshRenderer");
            if (renderer == null) {
                return;
            }

            var source = renderer.material.mainTexture;
            if (source == null) {
                UnityEngine.Debug.LogError("Could not find background texture");
                return;
            }

            RenderTexture target = RenderTexture.GetTemporary(source.width, source.height);

            Graphics.CopyTexture(source, 0, 0, 0, 0, source.width, source.height, target, 0, 0, 0, 0);

            TextureScale.Bilinear(bgImage, (int)(target.height * 0.71388), target.height);

            Graphics.CopyTexture(bgImage, 0, 0, 0, 0, bgImage.width, bgImage.height, target, 0, 0, target.width / 2 - bgImage.width / 2, 0);

            renderer.material.SetTexture(MainTex, target.ToTexture2D());
            RenderTexture.ReleaseTemporary(target);
        }

        private void SetUpNameTranslation() {
            var xUnityAutoTranslator = GameObject.Find("___XUnityAutoTranslator");

            if (xUnityAutoTranslator != null) {
                var translationComponents = xUnityAutoTranslator.GetComponents<Component>();
                var autoTranslator = translationComponents.FirstOrDefault(cm => cm.GetType().Name == "AutoTranslationPlugin");
                if (autoTranslator != null) {
                    var methodInfo = autoTranslator.GetType().GetMethod("TryTranslate", BindingFlags.NonPublic | BindingFlags.Instance, null,
                        new[] { typeof(string), typeof(int), typeof(string).MakeByRefType() }, null);

                    if (methodInfo != null) {
                        translateName = (TranslateName)Delegate.CreateDelegate(
                            typeof(TranslateName),
                            autoTranslator,
                            methodInfo);
                    }
                }
            }
        }

        private void CheckWearCustom() {
#if HS2
            studioCharaListUtilType = Type.GetType("WearCustom.StudioCharaListUtil, HS2WearCustom, Version=0.3.0.0, Culture=neutral, PublicKeyToken=null");
#elif AI
            studioCharaListUtilType = Type.GetType("AIWearCustom.StudioCharaListUtil, AIWearCustom, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null");
#endif

            if (studioCharaListUtilType == null) {
                wearCustomActive = false;
            } else {
                wearCustomActive = true;
            }
        }

        private void ToggleGender() {
            if (femaleActive) {
                genderButtonText.text = "Female";
                femaleActive = false;
                LoadDirList(CharLoaderPlugin.MaleBaseDir);
            } else {
                genderButtonText.text = "Male";
                femaleActive = true;
                LoadDirList(CharLoaderPlugin.FemaleBaseDir);
            }
        }

        private void CreateMenuButton(bool replaceStudioButtons) {
            GameObject original = GameObject.Find("StudioScene/Canvas Main Menu/01_Add/Scroll View Add Group/Viewport/Content/Chara Female");
            if (original == null) return;
            menuButton = Instantiate(original, original.transform.parent);
            menuButton.name = "Characters";
            Button component = menuButton.GetComponent<Button>();

            if (replaceStudioButtons) {
                menuButton.transform.SetSiblingIndex(0);
                original.transform.SetParent(null);
                GameObject.Find("StudioScene/Canvas Main Menu/01_Add/Scroll View Add Group/Viewport/Content/Chara Male").transform.SetParent(null);
            }

            var addCtrlType = typeof(AddButtonCtrl);
            var commonInfoType = addCtrlType.GetNestedType("CommonInfo", BindingFlags.NonPublic);

            var commonInfo = Activator.CreateInstance(commonInfoType);
            commonInfoType.GetField("obj").SetValue(commonInfo, GUI.gameObject);
            commonInfoType.GetField("button").SetValue(commonInfo, component);

            AddButtonCtrl addButtonCtrl = GameObject.Find("StudioScene/Canvas Main Menu/01_Add").GetComponent<AddButtonCtrl>();

            var ciField = addCtrlType.GetField("commonInfo", BindingFlags.NonPublic | BindingFlags.Instance);
            var commonInfoList = (Array)ciField.GetValue(addButtonCtrl);

            int lenght = commonInfoList.Length;
            var newList = (Array)Activator.CreateInstance(commonInfoList.GetType(), lenght + 1);
            for (int index = 0; index < lenght; index++) {
                newList.SetValue(commonInfoList.GetValue(index), index);
            }

            newList.SetValue(commonInfo, lenght);
            ciField.SetValue(addButtonCtrl, newList);

            component.onClick.ActuallyRemoveAllListeners();
            component.onClick.AddListener(() => addButtonCtrl.OnClick(lenght));
            menuButton.GetComponentInChildren<TMP_Text>().text = "Characters";

            closeAction = () => { addButtonCtrl.OnClick(lenght); };
        }

        private Action closeAction = () => { };


        private void LoadDirList(DirectoryInfo dirInfo) {
            selectedCharaFile = null;
            selectedCard = null;
            currentDir = dirInfo;
            var dirButtonPrefab = GUI.transform.Find("DirButton").GetComponent<Button>();

            var content = GUI.transform.Find("MainPanel/Body/DirView/Viewport/Content").GetComponent<Transform>();
            foreach (Transform child in content) {
                Destroy(child.gameObject);
            }

            Button parentDirButton = Instantiate(dirButtonPrefab.gameObject).GetComponent<Button>();
            parentDirButton.transform.Find("Text").GetComponent<Text>().text = "..";

            parentDirButton.onClick.AddListener((() => LoadDirList(dirInfo.Parent)));
            parentDirButton.gameObject.SetActive(true);
            parentDirButton.transform.SetParent(content.gameObject.transform, false);

            foreach (var subdir in dirInfo.GetDirectories()) {
                Button dirButton = Instantiate(dirButtonPrefab.gameObject).GetComponent<Button>();
                var text = dirButton.transform.Find("Text").GetComponent<Text>();
                text.text = subdir.Name;

                dirButton.onClick.AddListener(() => { LoadDirList(subdir); });

                dirButton.gameObject.SetActive(true);
                dirButton.transform.SetParent(content.gameObject.transform, false);
            }

            ListCharsInDir(dirInfo, false);
        }


        private void ListCharsInDir(DirectoryInfo dirInfo, bool variant) {
            Transform content;
            List<GameObject> panels;

            if (!variant) {
                content = GUI.transform.Find("MainPanel/Body/CharView/Viewport/Content").GetComponent<Transform>();
                panels = buttonPanels;
            } else {
                content = GUI.transform.Find("MainPanel/Body/VarView/Viewport/Content").GetComponent<Transform>();
                panels = variantPanels;
            }

            foreach (Transform child in content) {
                Destroy(child.gameObject);
            }

            panels.Clear();
            if (!dirInfo.Exists) {
                return;
            }

            foreach (var file in dirInfo.GetFiles("*.png").OrderByDescending(f => f.LastWriteTime)) {
                var fullname = "";
                int sex = 1;
                ChaFileControl chaFileControl = new ChaFileControl();
                if (chaFileControl.LoadCharaFile(file.FullName, 1, true)) {
                    fullname = chaFileControl.parameter.fullname;
                    sex = chaFileControl.parameter.sex;

                    if (translateName != null) {
                        bool translated = translateName(fullname, -1, out string translatedName);
                        if (translated) {
                            fullname = translatedName;
                        }
                    }
                }

                Button character = Instantiate(charButton).GetComponent<Button>();
                var fileData = File.ReadAllBytes(file.FullName);
                var img = character.GetComponent<RawImage>();
                var tex = new Texture2D(2, 2);
                tex.LoadImage(fileData);
                img.texture = tex;

                var txt = character.transform.Find("ProText").GetComponent<TextMeshProUGUI>();
                txt.text = fullname;


                if (file.FullName.Equals(selectedCharaFile?.FullName)) {
                    img.color = selectedColor;
                    selectedCard = character;
                }

                character.gameObject.SetActive(true);
                character.transform.SetParent(content.gameObject.transform, false);

                panels.Add(character.transform.Find("ButtonPanel").gameObject);

                var loadButton = character.transform.Find("ButtonPanel/LoadButton").GetComponent<Button>();
                var replaceButton = character.transform.Find("ButtonPanel/ReplaceButton").GetComponent<Button>();
                loadButton.onClick.AddListener(() => LoadChara(file.FullName));
                replaceButton.onClick.AddListener(() => ReplaceChara(file.FullName, sex));

                if (wearCustomActive) {
                    var anatomyButton = character.transform.Find("ButtonPanel/AnatomyButton").GetComponent<Button>();
                    anatomyButton.gameObject.SetActive(true);
                    anatomyButton.onClick.AddListener(() => CallWearCustom(file.FullName, anatomy));
                    var outfitBUtton = character.transform.Find("ButtonPanel/OutfitButton").GetComponent<Button>();
                    outfitBUtton.gameObject.SetActive(true);
                    outfitBUtton.onClick.AddListener(() => CallWearCustom(file.FullName, outfit));
                    var clothesButton = character.transform.Find("ButtonPanel/ClothesButton").GetComponent<Button>();
                    clothesButton.gameObject.SetActive(true);
                    clothesButton.onClick.AddListener(() => CallWearCustom(file.FullName, clothes));
                    var accButton = character.transform.Find("ButtonPanel/AccessoriesButton").GetComponent<Button>();
                    accButton.gameObject.SetActive(true);
                    accButton.onClick.AddListener(() => CallWearCustom(file.FullName, accessories));
                    var bodyButton = character.transform.Find("ButtonPanel/BodyButton").GetComponent<Button>();
                    bodyButton.gameObject.SetActive(true);
                    bodyButton.onClick.AddListener(() => CallWearCustom(file.FullName, body));
                    var faceButton = character.transform.Find("ButtonPanel/FaceButton").GetComponent<Button>();
                    faceButton.gameObject.SetActive(true);
                    faceButton.onClick.AddListener(() => CallWearCustom(file.FullName, face));
                    var hairButton = character.transform.Find("ButtonPanel/HairButton").GetComponent<Button>();
                    hairButton.gameObject.SetActive(true);
                    hairButton.onClick.AddListener(() => CallWearCustom(file.FullName, hair));
                }

                character.onClick.AddListener(() => {
                    if (selectedCard != null) {
                        selectedCard.GetComponent<RawImage>().color = Color.white;
                    }

                    character.GetComponent<RawImage>().color = selectedColor;
                    selectedCharaFile = file;
                    selectedCard = character;
                    selectedIsVariant = variant;
                    foreach (var lp in buttonPanels.Concat(variantPanels)) {
                        if (lp != null)
                            lp.SetActive(false);
                    }

                    var panel = character.transform.Find("ButtonPanel").gameObject;
                    panel.SetActive(true);
                    var variantDirInfo = new DirectoryInfo((femaleActive ? CharLoaderPlugin.FemaleBaseDir : CharLoaderPlugin.MaleBaseDir).FullName + @"\" +
                                                           CharLoaderPlugin.VariantsDirName + @"\" + fullname);
                    if (!variant) {
                        currentVariantDir = null;
                        ListCharsInDir(variantDirInfo, true);
                    } else {
                        currentVariantDir = variantDirInfo;
                    }
                });
            }
        }

        private readonly bool[] anatomy = { true, true, true, false, false };
        private readonly bool[] outfit = { false, false, false, true, true };
        private readonly bool[] body = { true, false, false, false, false };
        private readonly bool[] face = { false, true, false, false, false };
        private readonly bool[] hair = { false, false, true, false, false };
        private readonly bool[] clothes = { false, false, false, true, false };
        private readonly bool[] accessories = { false, false, false, false, true };
        private static readonly int MainTex = Shader.PropertyToID("_MainTex");

        private void CallWearCustom(string fileFullName, bool[] loadState) {
            var chara00 = GameObject.Find("StudioScene/Canvas Main Menu/02_Manipulate/00_Chara");

            var studioCharaListUtil = chara00.GetComponent<StudioCharaListUtil>();
            if (studioCharaListUtil == null) {
                StudioCharaListUtil.Install();
                studioCharaListUtil = chara00.GetComponent<StudioCharaListUtil>();
            }

            var replaceCharaHairOnly = studioCharaListUtilType.GetField("replaceCharaHairOnly", BindingFlags.NonPublic | BindingFlags.Instance);
            var replaceCharaHeadOnly = studioCharaListUtilType.GetField("replaceCharaHeadOnly", BindingFlags.NonPublic | BindingFlags.Instance);
            var replaceCharaBodyOnly = studioCharaListUtilType.GetField("replaceCharaBodyOnly", BindingFlags.NonPublic | BindingFlags.Instance);
            var replaceCharaClothesOnly = studioCharaListUtilType.GetField("replaceCharaClothesOnly", BindingFlags.NonPublic | BindingFlags.Instance);
            var replaceCharaAccOnly = studioCharaListUtilType.GetField("replaceCharaAccOnly", BindingFlags.NonPublic | BindingFlags.Instance);

            var replaceFields = new[] { replaceCharaBodyOnly, replaceCharaHeadOnly, replaceCharaHairOnly, replaceCharaClothesOnly, replaceCharaAccOnly };
            for (int i = 0; i < replaceFields.Length; i++) {
                replaceFields[i].SetValue(studioCharaListUtil, loadState[i]);
            }

            var charaFileSortField = studioCharaListUtilType.GetField("charaFileSort", BindingFlags.NonPublic | BindingFlags.Instance);
            MethodInfo changeMetod =
                studioCharaListUtilType.GetMethod("ChangeChara", BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { }, null);

            CharaFileSort charaFileSort = charaFileSortField.GetValue(studioCharaListUtil) as CharaFileSort;
            charaFileSort.cfiList.Clear();
            var charaFileInfo = new CharaFileInfo(fileFullName);
            charaFileInfo.node = new ListNode();
            charaFileInfo.select = true;

            charaFileSort.cfiList.Add(charaFileInfo);
            charaFileSort.select = 0;
            changeMetod.Invoke(studioCharaListUtil, new object[] { });
        }

        private void ReplaceChara(string fileFullName, int sex) {
            OCIChar[] array = Singleton<GuideObjectManager>.Instance.selectObjectKey.Select(v => Studio.Studio.GetCtrlInfo(v) as OCIChar)
                .Where(v => v != null).Where(v => v.oiCharInfo.sex == sex).ToArray();
            int length = array.Length;
            for (int index = 0; index < length; ++index)
                array[index].ChangeChara(fileFullName);
            if (length > 0) {
                CloseWindowIfToggle();
            }
        }

        private void CloseWindowIfToggle() {
            if (closeToggle.isOn)
                closeAction.Invoke();
        }

        private void LoadChara(string fileFullName) {
            if (femaleActive) {
                Singleton<Studio.Studio>.Instance.AddFemale(fileFullName);
            } else {
                Singleton<Studio.Studio>.Instance.AddMale(fileFullName);
            }

            CloseWindowIfToggle();
        }

        private void MoveCardThroughTime(int replaceIndexOffset) {
            if (selectedCharaFile == null) {
                return;
            }

            DirectoryInfo activeDir;
            if (selectedIsVariant) {
                activeDir = currentVariantDir;
            } else {
                activeDir = currentDir;
            }

            var pngs = activeDir.GetFiles("*.png").OrderByDescending(f => f.LastWriteTime).ToList();
            int cardIndex = pngs.FindIndex(info => info.FullName.Equals(selectedCharaFile.FullName));
            int replaceAt = cardIndex + replaceIndexOffset;
            if (cardIndex == -1 || replaceAt < 0 || replaceAt >= pngs.Count) {
                return;
            }

            FileInfo replaceInfoAt = pngs[replaceAt];
            var replWriteTime = replaceInfoAt.LastWriteTime;
            var slctWriteTime = selectedCharaFile.LastWriteTime;

            if (slctWriteTime != replWriteTime) {
                File.SetLastWriteTime(replaceInfoAt.FullName, selectedCharaFile.LastWriteTime);
                File.SetLastWriteTime(selectedCharaFile.FullName, replaceInfoAt.LastWriteTime);
            } else {
                var bumpedTime = slctWriteTime.AddMilliseconds(-replaceIndexOffset);
                File.SetLastWriteTime(selectedCharaFile.FullName, bumpedTime);

                for (int bumpIndex = cardIndex + 2 * replaceIndexOffset;
                     bumpIndex > 0 && bumpIndex <= pngs.Count && pngs[bumpIndex].LastWriteTime == slctWriteTime;
                     bumpIndex += replaceIndexOffset) {
                    File.SetLastWriteTime(pngs[bumpIndex].FullName, bumpedTime);
                }
            }

            selectedCharaFile.Refresh();
            ListCharsInDir(activeDir, selectedIsVariant);
        }

        private IEnumerator ReplaceCardImage(FileInfo fileToReplace) {
            if (fileToReplace == null) {
                yield break;
            }

            ChaFileControl chafile = new ChaFileControl();
            chafile.LoadCharaFile(fileToReplace.FullName, femaleActive ? (byte)255 : (byte)0, true);

            var tex = new Texture2D(252, 352);

            var sysMenu = GameObject.Find("StudioScene/Canvas System Menu");
            sysMenu.SetActive(false);

            yield return new WaitForEndOfFrame();
            tex = TakeScreenshot();

            sysMenu.SetActive(true);

            if (tex == null) {
                yield break;
            }

            if (replaceCardGUI.transform.Find("ReplaceCardPanel/Resolution1").GetComponent<Toggle>().isOn) {
                TextureScale.Bilinear(tex, 252, 352);
            }

            if (replaceCardGUI.transform.Find("ReplaceCardPanel/Resolution2").GetComponent<Toggle>().isOn) {
                TextureScale.Bilinear(tex, 504, 704);
            }

            var chompedPng = tex.EncodeToPNG();

            chafile.pngData = chompedPng;
            var lastWriteTime = File.GetLastWriteTime(fileToReplace.FullName);

            chafile.SaveCharaFile(fileToReplace.FullName, femaleActive ? (byte)255 : (byte)0);
            if (CharLoaderPlugin.EnableCharacterReordering) {
                File.SetLastWriteTime(fileToReplace.FullName, lastWriteTime);
            }

            frameCanvas.gameObject.SetActive(false);
            ToggleBackground(false);
            ListCharsInDir(currentDir, false);
        }

        public Texture2D TakeScreenshot() {
            int height = Screen.height;
            int width = (int)(height * 0.71388);
            var startX = (Screen.width - width) / 2;
            var startY = 0;
            var tex = new Texture2D(width, height, TextureFormat.RGB24, false);

            Rect rex = new Rect(startX, startY, width, height);

            tex.ReadPixels(rex, 0, 0);
            tex.Apply();

            return tex;
        }

        public void ToggleActive() {
            GUI?.gameObject.SetActive(!GUI.gameObject.activeInHierarchy);
        }
    }
}