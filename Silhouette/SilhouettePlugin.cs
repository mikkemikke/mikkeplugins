﻿using System.Linq;
using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using Studio;
using UnityEngine;

namespace Silhouette {
    [BepInPlugin(GUID, "Silhouette plugin", VERSION)]
    [BepInProcess("StudioNEOV2")]
    public class SilhouettePlugin : BaseUnityPlugin {
        public const string GUID = "mikke.Silhouette";
        internal const string VERSION = "1.0";

        private static bool justReloaded = false;

        private static int femaleCounter = 0;
        private static int futaCounter = 0;
        private static int maleCounter = 0;

        private static ConfigEntry<KeyboardShortcut> ShortKey { get; set; }
        private static ConfigEntry<bool> EnablePlugin;
        private static ConfigEntry<bool> EnableSilhouetteColors;


        private void Start() {
            EnablePlugin = Config.Bind("Config", "Enable silhouette plugin", true);
            ShortKey = Config.Bind("Config", "Toggle silhouettes", new KeyboardShortcut(KeyCode.W));
            EnableSilhouetteColors = Config.Bind("Config", "Enable silhouette coloring", true,
                "Set the silhouette color of newly added characters to a gender appropriate shade");
        }

        private void Awake() {
            Harmony.CreateAndPatchAll(typeof(SilhouettePlugin));
        }

        [HarmonyPostfix, HarmonyPatch(typeof(Studio.Studio), nameof(Studio.Studio.LoadSceneCoroutine))]
        internal static void LoadSceneCoroutinePostfix() {
            justReloaded = true;
        }

        [HarmonyPostfix, HarmonyPatch(typeof(Studio.Studio), nameof(Studio.Studio.LoadScene))]
        internal static void LoadScenePostfix() {
            justReloaded = true;
        }

        [HarmonyPostfix, HarmonyPatch(typeof(Studio.Studio), nameof(Studio.Studio.ImportScene))]
        internal static void ImportScenePostfix() {
            justReloaded = true;
        }

        [HarmonyPostfix, HarmonyPatch(typeof(Studio.Studio), nameof(Studio.Studio.InitScene))]
        internal static void InitScenePostfix() {
            femaleCounter = 0;
            futaCounter = 0;
            maleCounter = 0;
        }

        [HarmonyPostfix, HarmonyPatch(typeof(AddObjectFemale), nameof(AddObjectFemale.Add), typeof(string))]
        internal static void AddFemalePostfix(ref OCICharFemale __result, string _path) {
            if (!EnablePlugin.Value || !EnableSilhouetteColors.Value) {
                return;
            }

            if (justReloaded) {
                CountCharactersInScene();
            }

            if (__result.sex == 0) {
                //bug compatible with CharLoader
                SetMaleSimpleColor(__result);
            } else {
                __result.SetSimpleColor(__result.female.fileParam.futanari ? ProceduralFutaColor() : ProceduralFemaleColor());
                if (forceSilhouettesOn) __result.SetVisibleSimple(true);
            }
        }

        [HarmonyPostfix, HarmonyPatch(typeof(AddObjectMale), nameof(AddObjectMale.Add), typeof(string))]
        internal static void AddMalePostfix(ref OCICharMale __result, string _path) {
            SetMaleSimpleColor(__result);
        }

        private static void SetMaleSimpleColor(OCIChar ociChar) {
            if (!EnablePlugin.Value || !EnableSilhouetteColors.Value) {
                return;
            }

            if (justReloaded) {
                CountCharactersInScene();
            }

            ociChar.SetSimpleColor(ProceduralMaleColor());
            if (forceSilhouettesOn) ociChar.SetVisibleSimple(true);
        }

        private static void CountCharactersInScene() {
            //This counts the character that was just added so it's not completely accurate, but it doesn't matter.
            int females = 0;
            int futas = 0;
            int males = 0;
            foreach (var objectCtrlInfo in Studio.Studio.Instance.dicInfo.Values) {
                if (objectCtrlInfo is OCICharFemale femaleChar) {
                    if (femaleChar.female.fileParam.futanari) {
                        futas++;
                    } else {
                        females++;
                    }
                } else if (objectCtrlInfo is OCICharMale) {
                    males++;
                }
            }

            femaleCounter = females;
            futaCounter = futas;
            maleCounter = males;
            justReloaded = false;
        }


        private static Color[] femaleColors = {
            new Color(1f, 0.50f, 1f),
            new Color(1f, 0.55f, 1f),
            new Color(1f, 0.30f, 1f),
            new Color(1f, 0.60f, 1f),
            new Color(1f, 0.65f, 1f),
            new Color(0.8f, 0.50f, 1f),
            new Color(0.8f, 0.55f, 1f),
            new Color(0.8f, 0.30f, 1f),
            new Color(0.8f, 0.60f, 1f),
            new Color(0.8f, 0.65f, 1f),
            new Color(1f, 0.50f, 0.7f),
            new Color(1f, 0.55f, 0.7f),
            new Color(1f, 0.30f, 0.7f),
            new Color(1f, 0.60f, 0.7f),
            new Color(1f, 0.65f, 0.7f),
        };

        private static Color[] maleColors = {
            new Color(0f, 0f, 1f),
            new Color(0f, 0.5f, 1f),
            new Color(0.4f, 0.4f, 1f),
            new Color(0.5f, 0.6f, 1f),
            new Color(0.4f, 0.5f, 0.7f),
            new Color(0f, 0.4f, 1f),
            new Color(0.5f, 0.6f, 0.7f),
            new Color(0.2f, 0.4f, 0.8f),
            new Color(0.4f, 0.6f, 0.8f),
            new Color(0.3f, 0.4f, 0.75f),
            new Color(0f, 0.6f, 1f),
        };

        private static Color[] futaColors = {
            new Color(0.4f, 1f, 1f),
            new Color(0.6f, 1f, 1f),
            new Color(0.4f, 0.8f, 0.7f),
            new Color(0.4f, 1f, 0.7f),
            new Color(0.4f, 0.8f, 0.8f),
        };

        private static Color ProceduralFemaleColor() {
            return femaleColors[femaleCounter++ % femaleColors.Length];
        }

        private static Color ProceduralFutaColor() {
            return futaColors[futaCounter++ % futaColors.Length];
        }

        private static Color ProceduralMaleColor() {
            return maleColors[maleCounter++ % maleColors.Length];
        }

        private bool initialized;

        private void OnLevelWasLoaded(int level) {
            if (Studio.Studio.Instance != null && !initialized) {
                Studio.Studio.Instance.treeNodeCtrl.onSelect += o => { ProcessSilhouette(); };
                Studio.Studio.Instance.treeNodeCtrl.onDeselect += o => { ProcessSilhouette(); };
                Studio.Studio.Instance.treeNodeCtrl.onSelectMultiple += () => { ProcessSilhouette(); };
                initialized = true;
            }
        }

        private void ProcessSilhouette() {
            if (!forceSilhouettesOn && silhouettesOn) ToggleSilhouette();
        }

        private static bool silhouettesOn;
        private static bool forceSilhouettesOn;
        private float keyDownTime = 0f;
        private const float Yard = 1000000000f;

        private void Update() {
            if (EnablePlugin.Value) {
                if (ShortKey.Value.IsDown()) {
                    keyDownTime = Time.time;
                } else if (ShortKey.Value.IsPressed()) {
                    if (Time.time - keyDownTime > 1) {
                        keyDownTime += Yard;
                        if (forceSilhouettesOn) {
                            forceSilhouettesOn = !forceSilhouettesOn;
                            ToggleSilhouette();
                        } else {
                            forceSilhouettesOn = true;
                            ToggleSilhouette();
                        }
                    }
                }

                if (ShortKey.Value.IsUp()) {
                    if (keyDownTime > Yard) {
                    } else {
                        if (forceSilhouettesOn) {
                            forceSilhouettesOn = !forceSilhouettesOn;
                        } else {
                            silhouettesOn = !silhouettesOn;
                        }

                        ToggleSilhouette();
                    }
                }
            }
        }

        private OCIChar lastSelectedChar;

        private void ToggleSilhouette() {
            bool forceSilhouettes = forceSilhouettesOn;
            var dicInfos = Studio.Studio.Instance.dicInfo;
            var selectedObjs = Studio.Studio.Instance.treeNodeCtrl.selectObjectCtrl;

            if (selectedObjs.Length > 0 && selectedObjs[0] is OCIChar currCar) {
                lastSelectedChar = currCar;
            }

            TreeNodeObject parentCharNode = null;
            if (Studio.Studio.Instance.treeNodeCtrl.selectNode != null) {
                parentCharNode = FindParentCharNode(Studio.Studio.Instance.treeNodeCtrl.selectNode);
            }

            foreach (var dicInfo in dicInfos) {
                if (dicInfo.Value is OCIChar ociChar) {
                    if (ociChar.oiCharInfo.simpleColor.a < 1f) {
                        continue; //Ignore all characters with translucent monocolor
                    }

                    if (forceSilhouettes) {
                        ociChar.SetVisibleSimple(true);
                    } else if (selectedObjs.Contains(ociChar)) {
                        ociChar.SetVisibleSimple(false);
                    } else if (dicInfo.Key.Equals(parentCharNode) && parentCharNode != null) {
                        if (Studio.Studio.Instance.dicInfo.TryGetValue(parentCharNode, out var objectCtrlInfo) && objectCtrlInfo is OCIChar parentChar) {
                            parentChar.SetVisibleSimple(false);
                        }
                    } else if (ociChar.Equals(lastSelectedChar)) {
                        ociChar.SetVisibleSimple(false);
                    } else if (ociChar.oiCharInfo.simpleColor.Equals(Color.white)) {
                        ociChar.SetVisibleSimple(false);
                    } else {
                        ociChar.SetVisibleSimple(silhouettesOn);
                    }
                }
            }
        }

        private TreeNodeObject FindParentCharNode(TreeNodeObject selectNode) {
            if (Studio.Studio.Instance.dicInfo.TryGetValue(selectNode, out var objectCtrlInfo) && objectCtrlInfo is OCIChar) {
                return null;
            }

            while (true) {
                if (selectNode.parent == null) return null;
                selectNode = selectNode.parent;
                if (Studio.Studio.Instance.dicInfo.TryGetValue(selectNode, out var oci) && oci is OCIChar) {
                    return selectNode;
                }
            }
        }
    }
}